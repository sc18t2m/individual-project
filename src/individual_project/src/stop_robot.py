#!/usr/bin/env python

import rospy
from geometry_msgs.msg import Twist

def publisher():
	rospy.init_node('Walker', anonymous=True)
	pub = rospy.Publisher('cmd_vel', Twist, queue_size=10)
	rate = rospy.Rate(10) #10hz
	dv = Twist() # desired velocity
	dv.linear.x = 0
	dv.angular.z = 0
	for i in range(10):
		pub.publish(dv)
		rate.sleep()
		

if __name__ == "__main__":
	try:
		publisher()
	except rospy.ROSInterruptException:
		pass

