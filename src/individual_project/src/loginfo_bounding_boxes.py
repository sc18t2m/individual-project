#!/usr/bin/env python

import rospy
from darknet_ros_msgs.msg import BoundingBoxes

def loginfo():
	rospy.init_node('loginfo')
	rospy.Subscriber('darknet_ros/bounding_boxes', BoundingBoxes, console_loginfo)
	rospy.spin()

def console_loginfo(data):
	rospy.loginfo(str(data.bounding_boxes[0].xmin))
		

if __name__ == "__main__":
	loginfo()
