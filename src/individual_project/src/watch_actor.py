#!/usr/bin/env python

import rospy
from geometry_msgs.msg import Twist
from darknet_ros_msgs.msg import BoundingBoxes

def publisher():
	rospy.init_node('Walker', anonymous=True)
	rospy.Subscriber('darknet_ros/bounding_boxes', BoundingBoxes, turn)
	rospy.spin()

def turn(data):
	for bb in data.bounding_boxes:
		bounding_box_center = (bb.xmax + bb.xmin) / 2
		rospy.loginfo("Class: " + bb.Class + "\tCenter:" + str(bounding_box_center))
	for bounding_box in data.bounding_boxes:
		if (bounding_box.Class != "person"):
			return
		pub = rospy.Publisher('cmd_vel', Twist, queue_size=10)
		rate = rospy.Rate(10) #10hz
		dv = Twist() # desired velocity
		bounding_box_center = (bounding_box.xmax + bounding_box.xmin) / 2
		if (bounding_box_center > 340):
			rospy.loginfo("RRRRRRRRRRR")
			dv.angular.z = -0.3
		elif (bounding_box_center < 270):
			rospy.loginfo("LLLLLLLLLLL")
			dv.angular.z = 0.3
		else:
			rospy.loginfo("MMMMMMMMMMM")
			dv.angular.z = 0

		pub.publish(dv)
			
		
if __name__ == "__main__":
	try:
		publisher()
	except rospy.ROSInterruptException:
		pass

