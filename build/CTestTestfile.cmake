# CMake generated Testfile for 
# Source directory: /home/tomas/catkin_ws/src
# Build directory: /home/tomas/catkin_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("turtlebot3_simulations/turtlebot3_simulations")
subdirs("individual_project")
subdirs("darknet_ros/darknet_ros_msgs")
subdirs("darknet_ros/darknet_ros")
subdirs("turtlebot3_simulations/turtlebot3_fake")
subdirs("turtlebot3_simulations/turtlebot3_gazebo")
